def call(String dockerRepoName, String imageName, portNum) {
    pipeline {
        agent any

        stages {
            stage('Build') {
                steps {
                    sh 'pip install -r requirements.txt --break-system-packages'
                }
            }
            
            stage('Python Lint') {
                steps {
                    sh 'pylint --fail-under=5 $(find . -name "*.py")'
                }
            }

            stage('Package') {
                steps {
                    withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                        sh "docker login -u 'sandyto7' -p '${TOKEN}' docker.io"
                        sh "docker build -t ${dockerRepoName}:latest --tag sandyto7/${dockerRepoName}:${imageName} ."
                        sh "docker push sandyto7/${dockerRepoName}:${imageName}"
                    }
                }
            }

            stage('Trivy Scan') {
                steps {
                    script {
                        sh 'which trivy || { echo "Installing Trivy"; curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh; }'
                        // Scanning without failing the build
                        sh "trivy image --severity HIGH,CRITICAL sandyto7/${dockerRepoName}:${imageName}"
                    }
                }
            }

            stage('Deploy') {
                steps {
                    sshagent(credentials : ['sshkey']) {
                        sh """
                            ssh -o StrictHostKeyChecking=no azureuser@acit-3855-lab-6a.westus3.cloudapp.azure.com 'docker-compose -f /home/azureuser/assign-3/deployment/docker-compose.yml down'
                            ssh -o StrictHostKeyChecking=no azureuser@acit-3855-lab-6a.westus3.cloudapp.azure.com 'docker pull sandyto7/${dockerRepoName}:${imageName}'
                            ssh -o StrictHostKeyChecking=no azureuser@acit-3855-lab-6a.westus3.cloudapp.azure.com 'docker-compose -f /home/azureuser/assign-3/deployment/docker-compose.yml up -d'
                        """
                    }
                }
            }
        }
    }
}
